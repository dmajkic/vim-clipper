" fmt.vim: Vim command to format Clipper files with hbformatgofmt.
" Influenced by fmt.vim for Go which is
" Copyright by The Go Authors. All rights reserved.
" Use of this source code is governed by a BSD-style
" license that can be found in the LICENSE file.
"
" This filetype plugin add a new commands for Clipper buffers:
"
"   :Fmt
"
"       Filter the current Clipper buffer through hbformat.
"       It tries to preserve cursor position and avoids
"       replacing the buffer with stderr output.
"

command! -buffer Fmt call s:HbFormat()

function! s:HbFormat()
    let view = winsaveview()
    silent %!hbformat
    if v:shell_error
        let errors = []
        for line in getline(1, line('$'))
            let tokens = matchlist(line, '^\(.\{-}\):\(\d\+\):\(\d\+\)\s*\(.*\)')
            if !empty(tokens)
                call add(errors, {"filename": @%,
                                 \"lnum":     tokens[2],
                                 \"col":      tokens[3],
                                 \"text":     tokens[4]})
            endif
        endfor
        if empty(errors)
            % | " Couldn't detect hbformat error format, output errors
        endif
        undo
        if !empty(errors)
            call setloclist(0, errors, 'r')
        endif
        echohl Error | echomsg "hbformat returned error" | echohl None
    endif
    call winrestview(view)
endfunction

" vim:ts=4:sw=4:et
